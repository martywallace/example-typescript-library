import { sum } from "./sum";

describe("sum", () => {
  test("It should correctly sum two values.", () => {
    expect(sum(5, 10)).toBe(15);
  });
});
